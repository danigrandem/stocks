import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './container/App';
import reducer from './reducers/index';
import mySaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
// mount it on the Store
const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware),
);
// then run the saga
sagaMiddleware.run(mySaga);
ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
