const { fromJS } = require('immutable');


const states = fromJS({
  companySymbol: '',
  company: {},
  companiesList: [],
  filteredCompanies: [],
  kw: '',
  range: '',
});

const reducers = (state = states, action) => {
  switch (action.type) {
    case 'SET_COMPANIES':
      return state.set('companiesList', fromJS(action.companies));
    case 'SET_FILTERED_COMPANIES':
      return state.set('filteredCompanies', fromJS(action.filteredCompanies)).set('kw', fromJS(action.kw));
    case 'SET_KW':
      return state.set('kw', fromJS(action.kw));
    case 'SET_COMPANY':
      return state.set('companySymbol', fromJS(action.companySymbol)).set('filteredCompanies', fromJS([]));
    case 'SET_COMPANY_OBJECT':
      return state.set('company', fromJS(action.company)).set('range', fromJS(action.range)).set('companySymbol', fromJS(action.companySymbol)).set('filteredCompanies', fromJS([]));
    default:
      return state;
  }
};

export default reducers;
