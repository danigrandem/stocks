import { call, put, takeLatest } from 'redux-saga/effects';
import Api from './Api';

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchCompanies() {
  const companies = yield call(Api.fetchCompanies);
  yield put({ type: 'SET_COMPANIES', companies });
}
function* fetchCompany(action) {
  const company = yield call(Api.fetchCompany, action.companySymbol, action.range);
  yield put({ type: 'SET_COMPANY_OBJECT', company, range: action.range, companySymbol: action.companySymbol });
}
/*
  Starts fetchUser on each dispatched `USER_FETCH_REQUESTED` action.
  Allows concurrent fetches of user.
*/
function* mySaga() {
  yield takeLatest('USER_FETCH_REQUESTED', fetchCompanies);
  yield takeLatest('COMPANY_FETCH_REQUESTED', fetchCompany);
}

export default mySaga;
