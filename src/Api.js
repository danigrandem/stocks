import axios from 'axios';

class Api {
  static fetchCompanies() {
    return axios.get('https://api.iextrading.com/1.0/ref-data/symbols')
      .then((result) => {
        const stocks = [];

        result.data.map(data => (
          stocks.push({
            date: data.date,
            iexId: data.iexId,
            isEnabled: data.isEnabled,
            name: data.name,
            symbol: data.symbol,
            type: data.type,
          })
        ));
        return stocks;
      });
  }
  static fetchCompany(companySymbol, range = '1m') {
    return axios.get(`https://api.iextrading.com/1.0/stock/market/batch?symbols=${companySymbol}&types=quote,chart&range=${range}&last=50`)
      .then(result => (result.data[companySymbol]));
  }
}

export default Api;
