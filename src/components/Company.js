import React from 'react';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs';
import styled from 'styled-components';
import Intervals from './Intervals';

const CompanyWrapper = styled.div`
  margin:10px;
`;
const Container = styled.div`
  overflow:hidden
`;
const DivError = styled.div`
  text-align:center;
  background:red;
  color:white;
  padding:10px;
`;
const Company = ({ company, companySymbol, range, onIntervalClick }) => {
  const labels = [];

  const datas = [];
  company.get('chart').map(c => datas.push(c.get('close')));
  company.get('chart').map(c => labels.push(c.get('label')));
  const data = {
    labels,
    datasets: [
      {
        label: `${company.getIn(['quote', 'companyName'])}`,
        fillColor: 'rgba(220,220,220,0.2)',
        strokeColor: 'rgba(220,220,220,1)',
        pointColor: 'rgba(220,220,220,1)',
        pointStrokeColor: '#fff',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(220,220,220,1)',
        data: datas,
      },
    ],
  };
  return (
    <Container>
      <CompanyWrapper>
        <h1>{company.getIn(['quote', 'companyName'])}</h1>
        {(datas.length === 0) ?
          <DivError>Error importing chart</DivError> :
          <Line data={data} options={{ responsive: true, showXLabels: 10 }} width="600" height="250" />}
      </CompanyWrapper>
      <Intervals onIntervalClick={onIntervalClick} companySymbol={companySymbol} range={range} />
    </Container>
  );
};

Company.propTypes = {
  onIntervalClick: PropTypes.func.isRequired,
  companySymbol: PropTypes.string.isRequired,
  company: PropTypes.object.isRequired,
  range: PropTypes.string.isRequired,
};

export default Company;
