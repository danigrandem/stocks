import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


const FilterContainer = styled.div`
  width:100%;
  background:#f4f0f0
`;
const Filter = styled.div`
  width:100%;
  border-bottom:1px solid black;
  padding:5px
`;
const A = styled.a`
  text-decoration:none;
  color:rgb(125, 149, 182);
  &:hover {
    color:black;
  }
`;
const Searcher = ({ filteredCompanies, onCompanyClick }) => {
  const filtered = filteredCompanies
    .filter((i, index) => (index < 5))
    .map(i => (
      <Filter key={i.get('symbol')} >
        <A onClick={() => onCompanyClick(i.get('symbol'))} href="#body">{i.get('name')}</A>
      </Filter>
    ));
  return (
    <FilterContainer>
      {filtered}
    </FilterContainer>
  );
};

Searcher.propTypes = {
  onCompanyClick: PropTypes.func.isRequired,
  filteredCompanies: PropTypes.object.isRequired,
};

export default Searcher;
