import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const IntervalContainer = styled.div`
  background:rgb(6,103,208);
  padding:5px
`;
const A = styled.a`
  text-decoration:none;
  color:${props => props.selected ? 'black' : 'white'};
  padding:5px
`;
const Intervals = ({ onIntervalClick, companySymbol, range }) => (
  <IntervalContainer>
    {(range === '5d') ?
      <A href="#body" onClick={() => onIntervalClick(companySymbol, '5d')} selected>5d</A> :
      <A href="#body" onClick={() => onIntervalClick(companySymbol, '5d')} selected={false}>5d</A>}
    {(range === '1m') ?
      <A href="#body" onClick={() => onIntervalClick(companySymbol, '1m')} selected>1m</A> :
      <A href="#body" onClick={() => onIntervalClick(companySymbol, '1m')} selected={false}>1m</A>}
    {(range === '6m') ?
      <A href="#body" onClick={() => onIntervalClick(companySymbol, '6m')} selected>6m</A> :
      <A href="#body" onClick={() => onIntervalClick(companySymbol, '6m')} selected={false}>6m</A>}
  </IntervalContainer>
);

Intervals.propTypes = {
  onIntervalClick: PropTypes.func.isRequired,
  companySymbol: PropTypes.string.isRequired,
  range: PropTypes.string.isRequired,
};

export default Intervals;
