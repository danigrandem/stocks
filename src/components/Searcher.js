import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
  padding:10px;
  background:rgb(6, 103, 208)
`;
const Input = styled.input`
  width:100%;
  height:30px
`;
const Searcher = ({ kw, onInputChange }) => (
  <Container>
    <Input id="search" placeholder="Search a company" type="text" name="" defaultValue={kw} onChange={onInputChange} />
  </Container>
);

Searcher.propTypes = {
  onInputChange: PropTypes.func.isRequired,
  kw: PropTypes.string.isRequired,
};
export default Searcher;
