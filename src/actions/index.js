export const setCompanies = companies => (
  {
    type: 'SET_COMPANIES',
    companies,
  }
);
export const userFetch = () => (
  {
    type: 'USER_FETCH_REQUESTED',
  }
);
export const setKw = kw => (
  {
    type: 'SET_KW',
    kw,
  }
);
export const setCompany = companySymbol => (
  {
    type: 'SET_COMPANY',
    companySymbol,
  }
);
export const setCompanyObject = (company, range, companySymbol) => (
  {
    type: 'SET_COMPANY_OBJECT',
    company,
    range,
    companySymbol,
  }
);
export const companyFetch = (companySymbol, range = '1m') => (
  {
    type: 'COMPANY_FETCH_REQUESTED',
    companySymbol,
    range,
  }
);
export const setFilteredCompanies = (filteredCompanies, kw) => (
  {
    type: 'SET_FILTERED_COMPANIES',
    filteredCompanies,
    kw,
  }
);
