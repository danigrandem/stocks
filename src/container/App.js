import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import './App.css';
import { setCompanies, setFilteredCompanies, setKw, setCompany } from '../actions/index';
import Searcher from '../components/Searcher';
import Company from '../components/Company';
import CompaniesFilter from '../components/CompaniesFilter';

class App extends Component {
  componentDidMount() {
    this.props.dispatch({ type: 'USER_FETCH_REQUESTED' });
    window.addEventListener('mousedown', this.handleClick, false);
  }
  componentDidUnMount() {
    this.props.dispatch({ type: 'USER_FETCH_REQUESTED' });
    window.removeEventListener('mousedown', this.handleClick, false);
  }
  handleClick = (e) => {
    if (this.node.contains(e.target)) {
      return;
    }
    this.props.setFilteredCompanies([], '');
  }
  onInputChange = (event) => {
    const { companiesList } = this.props;
    let filteredCompanies = [];
    filteredCompanies = companiesList.filter(element => element.get('name').toLowerCase().includes(event.target.value.toLowerCase()));
    this.props.setFilteredCompanies(filteredCompanies, event.target.value);
  }
  onCompanyClick = (companySymbol) => {
    const { companiesList } = this.props;
    const company = companiesList.find(c => (
      c.get('symbol') === companySymbol
    ));
    this.props.dispatch({ type: 'COMPANY_FETCH_REQUESTED', companySymbol: company.get('symbol'), range: '1m' });
  }
  onIntervalClick = (companySymbol, range) => {
    const { companiesList } = this.props;
    const company = companiesList.find(c =>
      c.get('symbol') === companySymbol,
    );
    this.props.dispatch({ type: 'COMPANY_FETCH_REQUESTED', companySymbol: company.get('symbol'), range });
  }
  render() {
    const { kw, filteredCompanies, companySymbol, company, range } = this.props;
    return (
      <div >
        <div ref={(node) => { this.node = node; return this.node; }}>
          <Searcher onInputChange={this.onInputChange} kw={kw} filteredCompanies={filteredCompanies} />
          {(filteredCompanies.size > 0) ? <CompaniesFilter onCompanyClick={this.onCompanyClick} filteredCompanies={filteredCompanies} /> : ''}
        </div>
        {(company.size > 0) ? <Company company={company} companySymbol={companySymbol} range={range} onIntervalClick={this.onIntervalClick} /> : ''}
      </div>
    );
  }
}
App.propTypes = {
  setFilteredCompanies: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  filteredCompanies: PropTypes.object.isRequired,
  companiesList: PropTypes.object.isRequired,
  kw: PropTypes.string.isRequired,
  companySymbol: PropTypes.string.isRequired,
  company: PropTypes.object.isRequired,
  range: PropTypes.string.isRequired,
};
App.defaultProps = {
  kw: '',
};
const mapStateToProps = state => (
  {
    companiesList: state.get('companiesList'),
    filteredCompanies: state.get('filteredCompanies'),
    companySymbol: state.get('companySymbol'),
    company: state.get('company'),
    range: state.get('range'),
  }
);
const mapDispatchToProps = dispatch => (
  {
    setCompanies: (type, companies) => dispatch(setCompanies(type, companies)),
    setFilteredCompanies: (type, filteredCompanies, kw) => dispatch(setFilteredCompanies(type, filteredCompanies, kw)),
    setKw: (type, kw) => dispatch(setKw(type, kw)),
    setCompany: (type, companySymbol) => dispatch(setCompany(type, companySymbol)),
    dispatch,
  }
);


// Connected Component
export default connect(mapStateToProps, mapDispatchToProps)(App);
