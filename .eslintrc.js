module.exports = {
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
      }
  },
  "extends": "airbnb",
  "env": {
    "browser": true,
    "node": true,
    "es6": true,
    "jest": true
  },
  "plugins": [
    "react",
  ],
  "rules": {
   "import/no-extraneous-dependencies": [2, { "devDependencies": true }],
   "react/forbid-prop-types": 0,
   "no-underscore-dangle": 0,
   "no-plusplus": 0,
   "array-callback-return": 0,
   "no-param-reassign": 0,
   "max-len": ["error", 200],
   "new-cap": 0,
   "react/sort-comp": 0,
   "no-unused-expressions": 0,
   "no-dupe-keys": 2,
   "react/jsx-no-duplicate-props": 2,
   "react/prop-types": 2,
   "import/named": 2,
   "react/require-extension": 0,
   "no-debugger": 1,
   "no-console": 1,
   "no-prototype-builtins": 0,
   "react/display-name": 0,
   "no-unused-vars": 1,
   "radix": 0,
   "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }]
   },
};
